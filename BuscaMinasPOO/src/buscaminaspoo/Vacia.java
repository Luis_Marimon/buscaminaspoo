/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaminaspoo;

/**
 *
 * @author Luis
 */
public class Vacia extends Casilla {

    String empty = "-";

    public Vacia(boolean type) {
        super(type);
        type = false;

    }

    public String getEmpty() {
        return empty;
    }

    public void setEmpty(String empty) {
        this.empty = empty;
    }

}
