
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public class Poligon implements FigureFill {

    private Color color;
    private int px;
    private int[] cx;
    private int[] cy;
    private int rellena = 0;

    public Poligon(int sides, int[] cx, int[] cy) {
        this.px = sides;
        this.cx = cx;
        this.cy = cy;

    }

    @Override
    public void paint(Graphics graphics) {
        graphics.drawPolygon(cx, cy, px);
    }

    @Override
    public void fill(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillPolygon(cx, cy, px);
    }

    @Override
    public int rellenar() {
        return rellena;
    }

}
