
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public class Line implements Figure {

    private final int px, py, px2, py2;

    public Line(int x, int y, int x2, int y2) {
        this.px = x;
        this.py = y;
        this.px2 = x2;
        this.py2 = y2;

    }

    @Override
    public void paint(Graphics graphics) {

        graphics.drawLine(px, py, px2, py2);

    }

}
