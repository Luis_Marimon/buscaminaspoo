
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public class Square implements FigureFill {

    private Color color;
    private final int px, py, px2;
    private int rellena = 0;

    public Square(int x, int y, int lado, int rellena, Color color) {
        this.px = x;
        this.py = y;
        this.px2 = lado;
        this.rellena = rellena;
        this.color = color;

    }

    @Override
    public void paint(Graphics graphics) {

        graphics.drawRect(px, py, px2, px2);

    }

    @Override
    public void fill(Graphics graphics) {
        graphics.setColor(color);
        graphics.fillRect(px, py, px2, px2);
    }

    @Override
    public int rellenar() {
        return rellena;
    }

}
