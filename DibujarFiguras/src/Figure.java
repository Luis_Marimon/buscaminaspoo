
import java.awt.Graphics;

/**
 *
 * @author Luis
 */
public interface Figure {

    public void paint(Graphics g);

}
