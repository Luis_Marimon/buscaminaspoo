/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.LinkedList;
import java.util.Scanner;

public class Agendas {

    int year;
    int numP;
    LinkedList<Pag> paginas = new LinkedList<>();

    public Agendas(int año, int numPagina) {
        this.year = 2016;
        this.numP = numPagina;
    }

    public Agendas() {
    }

    ;
    
    public void NextP() {

        numP++;

        System.out.println("Estas en la pagina: " + numP);

        Agenda();
    }

    public void PrevP() {
        while (numP > 0) {
            numP--;

            System.out.println("Estas en la pagina: " + numP);

            Agenda();
        }

    }

    public void Agenda() {
        try {
            int choice = 0;

            System.out.println("----------------------");
            System.out.println("AGENDA");
            System.out.println("----------------------");
            System.out.println("1-Pasar Pagina");
            System.out.println("2-Retrocedes Pagina");
            System.out.println("3-Insertar Cita");
            System.out.println("4-Vaciar Agenda");
            System.out.println("5-Salir");
            System.out.println("-----------------------");

            Scanner sc = new Scanner(System.in);
            choice = sc.nextInt();

            switch (choice) {
                case 1:
                    NextP();
                    break;

                case 2:

                    PrevP();
                    break;

                case 3:
                    Pag p = new Pag();
                    p.insertarCita(this);
                    break;

                case 4:
                    Pag p1 = new Pag();
                    p1.vaciarAgenda(this);
                    break;

                case 5:
                    System.out.println("Adios");
                    break;
                default:
                    System.out.println("No has elegido una opción valida");

            }
        } catch (Exception e) {

        }

    }
}
