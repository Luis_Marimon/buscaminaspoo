/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.Calendar;

public class cita {

    int horaI;
    int horaF;
    String content;

    public cita(int horaInicio, int horaFinal, String contenido) {
        this.horaI = horaInicio;
        this.horaF = horaFinal;
        this.content = contenido;
    }

    public cita() {
    }

    ;
    
    public cita crearCita() {
        boolean correcto = false;

        do {
            try {
                String HoraI = "Introduce la hora de inicio";
                String HoraF = "Introduce la hora de finalizacion";

                horaI = Integer.parseInt(HoraI);
                horaF = Integer.parseInt(HoraF);
                content = "Introduce el contenido";

                correcto = true;
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Los datos introducidos no son correctos");
            }
        } while (correcto == false);

        cita c = new cita(horaI, horaF, content);

        return c;
    }
}
