
import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    static final String image_url = "http://www.google.es/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png";
    static final String url_html = "http://www.google.es/";

    public static void main(String[] args) throws IOException {
        Main.testIPs();

        URL url = new URL(image_url);
        URL urlHtml = new URL(url_html);

        createImage(url);

        Main.isPngFormat(url);

        System.out.println(Main.getUrlSource(urlHtml));
    }

    static void testIPs() {
        InetAddress[] addresses = new InetAddress[2];

        try {
            addresses[0] = InetAddress.getLoopbackAddress();
            addresses[1] = InetAddress.getByName("ioc.xtec.cat");

            for (InetAddress adress : addresses) {
                System.out.println("La IP es " + adress.getHostAddress());
                if (adress.isLoopbackAddress()) {
                    System.out.println(adress.getHostName() + " te una direccio loopback");
                } else {
                    System.out.println(adress.getHostName() + " no te una direccio loopback");
                }
            }

        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());

        }
    }

    static boolean isPngFormat(URL url) {
        boolean ret = false;
        try {
            URLConnection con = url.openConnection();
            String headerType = con.getContentType();
            String guessType = con.guessContentTypeFromName(url.getFile());
            ret = headerType.endsWith("png") || guessType.endsWith("png");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;
    }

    static void createImage(URL url) throws IOException {

        BufferedImage image = null;
        try {

            image = ImageIO.read(url);

            ImageIO.write(image, "png", new File("F:\\EscritorioHDD\\testscraping/out.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");

    }

    static String getUrlSource(URL url) throws IOException {

        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream(), "UTF-8"));
        //yc.getInputStream() --> HTML Bytes
        String inputLine;
        StringBuilder a = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            a.append(inputLine);
        }

        in.close();

        return a.toString();
    }
}
