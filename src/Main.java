
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author alumno
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        //INICIA Offers.java
        Offers o = new Offers();

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        // si se li posa 0 en lloc de 5, es inmediat
        //ScheduleAtFixedRate
        executor.scheduleAtFixedRate(o, 0, 5, TimeUnit.SECONDS);
        //THREAD SLEEP
        Thread.sleep(60000);
        executor.shutdown();

    }

}
